package com.demo.uploads;

import com.demo.uploads.service.uploadfiles.FilesStorageServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UploadImage implements CommandLineRunner {

    @Autowired
    FilesStorageServiceImp filesStorageService;

    public static void main(String[] args) {
        SpringApplication.run(UploadImage.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        filesStorageService.init();
    }
}
